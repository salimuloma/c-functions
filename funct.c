// using functions in c

#include <stdio.h>
#include <string.h>

// define the structure of the variables

struct Person{
    int age;
    char name[100];
    char school[100];
};

// declare the function to use the structure

void MyFunc(struct Person p){
    printf("My age is: %d\n", p.age);
    printf("My name is: %s\n",p.name);
    printf("I went to %s for my studies.\n",p.school);
}

// general function

int main(){
    struct Person person1={20, "salimu", "KATRICO"};
    struct Person person2={23, "Alima", "Kyamate"};

    MyFunc(person1);
    MyFunc(person2);

    return 0;
}